﻿namespace Duplom
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.tagAsHam_Button = new System.Windows.Forms.Button();
            this.tagAsSpam_Button = new System.Windows.Forms.Button();
            this.download_Button = new System.Windows.Forms.Button();
            this.settings_Button = new System.Windows.Forms.Button();
            this.mailText_TextBox = new System.Windows.Forms.TextBox();
            this.subjectMailList_ListBox = new System.Windows.Forms.ListBox();
            this.refresh_Button = new System.Windows.Forms.Button();
            this.inbox_RadioButton = new System.Windows.Forms.RadioButton();
            this.spam_RadioButton = new System.Windows.Forms.RadioButton();
            this.mailFolder_GroupBox = new System.Windows.Forms.GroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.mailFolder_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tagAsHam_Button
            // 
            this.tagAsHam_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.tagAsHam_Button, "tagAsHam_Button");
            this.tagAsHam_Button.Name = "tagAsHam_Button";
            this.tagAsHam_Button.UseVisualStyleBackColor = true;
            this.tagAsHam_Button.Click += new System.EventHandler(this.TagAsHam_Button_Click);
            // 
            // tagAsSpam_Button
            // 
            this.tagAsSpam_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.tagAsSpam_Button, "tagAsSpam_Button");
            this.tagAsSpam_Button.Name = "tagAsSpam_Button";
            this.tagAsSpam_Button.UseVisualStyleBackColor = true;
            this.tagAsSpam_Button.Click += new System.EventHandler(this.TagAsSpam_Button_Click);
            // 
            // download_Button
            // 
            this.download_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.download_Button, "download_Button");
            this.download_Button.Name = "download_Button";
            this.download_Button.UseVisualStyleBackColor = true;
            this.download_Button.Click += new System.EventHandler(this.Download_Button_Click);
            // 
            // settings_Button
            // 
            this.settings_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.settings_Button, "settings_Button");
            this.settings_Button.Name = "settings_Button";
            this.settings_Button.UseVisualStyleBackColor = true;
            this.settings_Button.Click += new System.EventHandler(this.Settings_Button_Click);
            // 
            // mailText_TextBox
            // 
            resources.ApplyResources(this.mailText_TextBox, "mailText_TextBox");
            this.mailText_TextBox.Name = "mailText_TextBox";
            // 
            // subjectMailList_ListBox
            // 
            resources.ApplyResources(this.subjectMailList_ListBox, "subjectMailList_ListBox");
            this.subjectMailList_ListBox.FormattingEnabled = true;
            this.subjectMailList_ListBox.Name = "subjectMailList_ListBox";
            this.subjectMailList_ListBox.DoubleClick += new System.EventHandler(this.SubjectMailList_ListBox_DoubleClick);
            // 
            // refresh_Button
            // 
            this.refresh_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.refresh_Button, "refresh_Button");
            this.refresh_Button.Name = "refresh_Button";
            this.refresh_Button.UseVisualStyleBackColor = true;
            this.refresh_Button.Click += new System.EventHandler(this.Refresh_Button_Click);
            // 
            // inbox_RadioButton
            // 
            this.inbox_RadioButton.Checked = true;
            this.inbox_RadioButton.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.inbox_RadioButton, "inbox_RadioButton");
            this.inbox_RadioButton.Name = "inbox_RadioButton";
            this.inbox_RadioButton.TabStop = true;
            this.inbox_RadioButton.UseVisualStyleBackColor = true;
            // 
            // spam_RadioButton
            // 
            this.spam_RadioButton.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.spam_RadioButton, "spam_RadioButton");
            this.spam_RadioButton.Name = "spam_RadioButton";
            this.spam_RadioButton.UseVisualStyleBackColor = true;
            // 
            // mailFolder_GroupBox
            // 
            this.mailFolder_GroupBox.Controls.Add(this.spam_RadioButton);
            this.mailFolder_GroupBox.Controls.Add(this.inbox_RadioButton);
            resources.ApplyResources(this.mailFolder_GroupBox, "mailFolder_GroupBox");
            this.mailFolder_GroupBox.Name = "mailFolder_GroupBox";
            this.mailFolder_GroupBox.TabStop = false;
            // 
            // progressBar1
            // 
            resources.ApplyResources(this.progressBar1, "progressBar1");
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.UseWaitCursor = true;
            // 
            // textBox1
            // 
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            resources.ApplyResources(this.textBox2, "textBox2");
            this.textBox2.Name = "textBox2";
            // 
            // Main_Form
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.mailFolder_GroupBox);
            this.Controls.Add(this.refresh_Button);
            this.Controls.Add(this.subjectMailList_ListBox);
            this.Controls.Add(this.mailText_TextBox);
            this.Controls.Add(this.settings_Button);
            this.Controls.Add(this.download_Button);
            this.Controls.Add(this.tagAsSpam_Button);
            this.Controls.Add(this.tagAsHam_Button);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Main_Form";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.mailFolder_GroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button tagAsHam_Button;
        private System.Windows.Forms.Button tagAsSpam_Button;
        private System.Windows.Forms.Button download_Button;
        private System.Windows.Forms.Button settings_Button;
        private System.Windows.Forms.TextBox mailText_TextBox;
        private System.Windows.Forms.Button refresh_Button;
        private System.Windows.Forms.RadioButton inbox_RadioButton;
        private System.Windows.Forms.RadioButton spam_RadioButton;
        private System.Windows.Forms.GroupBox mailFolder_GroupBox;
        private System.Windows.Forms.ProgressBar progressBar1;
        public System.Windows.Forms.ListBox subjectMailList_ListBox;
        public System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.TextBox textBox2;
    }
}

