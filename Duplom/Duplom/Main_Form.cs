﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization.Json;
using System.IO;
using MimeKit;

namespace Duplom
{
    public partial class Main_Form : Form
    {
        private Classifier core;

        public Main_Form()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            core = new Classifier();
            progressBar1.Maximum = 5;
            progressBar1.Value = 0;
            progressBar1.Visible = true;

            progressBar1.Value++;
            if (File.Exists($@"{Application.StartupPath}\db.json"))
            {
            using (FileStream stream = File.Open($@"{Application.StartupPath}\db.json", FileMode.Open))
            {
                var ser = new DataContractJsonSerializer(typeof(SaveInfo));
                var saveObj = (SaveInfo)ser.ReadObject(stream);
                core = new Classifier(saveObj);
            }
            } else { MessageBox.Show("База Даних не знайдена!"); }
            progressBar1.Value++;

            DirectoryInfo di = Directory.CreateDirectory($@"{Application.StartupPath}\tmp"); progressBar1.Value++; Directory.CreateDirectory($@"{Application.StartupPath}\Spam"); progressBar1.Value++; Directory.CreateDirectory($@"{Application.StartupPath}\Inbox"); progressBar1.Value++;
            progressBar1.Visible = false;
        }

        private void TagAsSpam_Button_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(mailText_TextBox.Text)) { core.TagSpam(mailText_TextBox.Text); }
            if (File.Exists($@"{Application.StartupPath}\Inbox\{subjectMailList_ListBox.SelectedItem.ToString()}.eml")) { File.Move($@"{Application.StartupPath}\Inbox\{subjectMailList_ListBox.SelectedItem.ToString()}.eml", $@"{Application.StartupPath}\Spam\{subjectMailList_ListBox.SelectedItem.ToString()}.eml"); }
        }

        private void TagAsHam_Button_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(mailText_TextBox.Text)) { core.TagHam(mailText_TextBox.Text); }
            if (File.Exists($@"{Application.StartupPath}\Spam\{subjectMailList_ListBox.SelectedItem.ToString()}.eml")) { File.Move($@"{Application.StartupPath}\Spam\{subjectMailList_ListBox.SelectedItem.ToString()}.eml", $@"{Application.StartupPath}\Inbox\{subjectMailList_ListBox.SelectedItem.ToString()}.eml"); }
        }

        private void Settings_Button_Click(object sender, EventArgs e)
        {
            Settings_Form newForm = new Settings_Form();
            newForm.StartPosition = FormStartPosition.CenterScreen;
            newForm.ShowDialog();
        }

        private void Download_Button_Click(object sender, EventArgs e)
        {
            progressBar1.Maximum = 3;
            progressBar1.Value = 0;
            progressBar1.Visible = true;

            DirectoryInfo di1 = new DirectoryInfo($"{Application.StartupPath}/Inbox");
            foreach (FileInfo file in di1.GetFiles())
            {
                file.Delete();
            }
            progressBar1.Value++;

            DirectoryInfo di2 = new DirectoryInfo($"{Application.StartupPath}/Spam");
            foreach (FileInfo file in di2.GetFiles())
            {
                file.Delete();
            }
            progressBar1.Value++;

            DirectoryInfo di3 = new DirectoryInfo($"{Application.StartupPath}/tmp");
            foreach (FileInfo file in di3.GetFiles())
            {
                file.Delete();
            }
            progressBar1.Value++;

            progressBar1.Visible = false;

            Download_Form newForm = new Download_Form();
            newForm.StartPosition = FormStartPosition.CenterScreen;
            newForm.Owner = this;
            newForm.ShowDialog();
        }

        private void SubjectMailList_ListBox_DoubleClick(object sender, EventArgs e)
        {
            if (subjectMailList_ListBox.SelectedItem != null)
            {
                if (inbox_RadioButton.Checked == true)
                {
                    var lmessage = MimeMessage.Load($@"{Application.StartupPath}\Inbox\{subjectMailList_ListBox.SelectedItem.ToString()}.eml");
                    var body = lmessage.BodyParts.OfType<TextPart>().FirstOrDefault();
                    mailText_TextBox.Text = body.Text;
                }
                else if (spam_RadioButton.Checked == true)
                {
                    var lmessage = MimeMessage.Load($@"{Application.StartupPath}\Spam\{subjectMailList_ListBox.SelectedItem.ToString()}.eml");
                    var body = lmessage.BodyParts.OfType<TextPart>().FirstOrDefault();
                    mailText_TextBox.Text = body.Text;
                }
            }
        }

        private void Refresh_Button_Click(object sender, EventArgs e)
        {
            if (inbox_RadioButton.Checked == true)
            {
                subjectMailList_ListBox.Items.Clear();
                DirectoryInfo dinfo = new DirectoryInfo($@"{Application.StartupPath}\Inbox");
                FileInfo[] Files = dinfo.GetFiles("*.eml");
                foreach (FileInfo file in Files)
                {
                    subjectMailList_ListBox.Items.Add(Path.GetFileNameWithoutExtension(file.Name));
                }
            }
            else if (spam_RadioButton.Checked == true)
            {
                subjectMailList_ListBox.Items.Clear();
                DirectoryInfo dinfo = new DirectoryInfo($@"{Application.StartupPath}\Spam");
                FileInfo[] Files = dinfo.GetFiles("*.eml");
                foreach (FileInfo file in Files)
                {
                    subjectMailList_ListBox.Items.Add(Path.GetFileNameWithoutExtension(file.Name));
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            progressBar1.Maximum = 7;
            progressBar1.Value = 0;
            progressBar1.Visible = true;

            if (core != null)
            {
                progressBar1.Value++;
                using (MemoryStream stream = new MemoryStream())
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(SaveInfo));
                    serializer.WriteObject(stream, core.GetSaveObj());
                    string json = Encoding.UTF8.GetString(stream.ToArray());
                    File.WriteAllText($@"{Application.StartupPath}\db.json", json);
                }
                progressBar1.Value++;
            }

            progressBar1.Value++;
            using (MemoryStream stream = new MemoryStream())
            {
                string json = "{\"hamless\":\"0,4\",\"idkless\":\"0,6\",\"idkmore\":\"0,4\",\"spammore\":\"0,6\"}";
                File.WriteAllText($@"{Application.StartupPath}\settings.json", json);
            }
            progressBar1.Value++;

            DirectoryInfo di1 = new DirectoryInfo($"{Application.StartupPath}/Inbox");
            foreach (FileInfo file in di1.GetFiles())
            {

                file.Delete();
            }
            progressBar1.Value++;

            DirectoryInfo di2 = new DirectoryInfo($"{Application.StartupPath}/Spam");
            foreach (FileInfo file in di2.GetFiles())
            {
                file.Delete();
            }
            progressBar1.Value++;

            DirectoryInfo di3 = new DirectoryInfo($"{Application.StartupPath}/tmp");
            foreach (FileInfo file in di3.GetFiles())
            {
                file.Delete();
            }
            progressBar1.Value++;
            progressBar1.Visible = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text == "SPAM") { core.TeachSpam(textBox1.Text); } else if (textBox2.Text == "HAM") { core.TeachHam(textBox1.Text); } else if (textBox2.Text == "IDK") { core.TeachIdk(textBox1.Text); }
        }
    }
}
