﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Globalization;

namespace Duplom
{
    public partial class Settings_Form : Form
    {
        public Settings_Form()
        {
            InitializeComponent();
        }

        private void CangeLimitsOfProbability_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (cangeLimitsOfProbability_CheckBox.Checked == true) { spamMoreThanLimit_MaskedTextBox.Enabled = true; hamLessThanLimit_MaskedTextBox.Enabled = true; idkMoreThanLimit_MaskedTextBox.Enabled = true; idkLessThanLimit_MaskedTextBox.Enabled = true; }
        }

        private void Save_Button_Click(object sender, EventArgs e)
        {
            CultureInfo temp_culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
            if (double.Parse(hamLessThanLimit_MaskedTextBox.Text) > double.Parse(spamMoreThanLimit_MaskedTextBox.Text)) { MessageBox.Show("Помилка!\nМежі не можуть бути такими!\n Число введене в поле спам більше за повинне бути меншим за число введене в поле не спам менше за"); hamLessThanLimit_MaskedTextBox.Clear(); spamMoreThanLimit_MaskedTextBox.Clear(); }
            if (double.Parse(hamLessThanLimit_MaskedTextBox.Text) < double.Parse(idkMoreThanLimit_MaskedTextBox.Text)) { MessageBox.Show("Помилка!\nМежі не можуть бути такими!\n Число введене в поле не спам менше за не може бути більшим за число в введене в поле сумнівне якщо більше за"); hamLessThanLimit_MaskedTextBox.Clear(); idkMoreThanLimit_MaskedTextBox.Clear(); }
            if (double.Parse(spamMoreThanLimit_MaskedTextBox.Text) > double.Parse(idkLessThanLimit_MaskedTextBox.Text)) { MessageBox.Show("Помилка!\nМежі не можуть бути такими!\n Число введене в поле спам більше за не може бути меншим за число введене в поле сумнівне якщо менше за"); spamMoreThanLimit_MaskedTextBox.Clear(); idkLessThanLimit_MaskedTextBox.Clear(); }
            if (double.Parse(idkMoreThanLimit_MaskedTextBox.Text) < double.Parse(idkLessThanLimit_MaskedTextBox.Text)) { MessageBox.Show("Помилка!\nМежі не можуть бути такими!\n Числов ведене в поле сумнівне якщо більше за не може бути меншим за число введене в поле сумнівне якщо менше за"); idkMoreThanLimit_MaskedTextBox.Clear(); idkLessThanLimit_MaskedTextBox.Clear(); }
            string _idkless = "0.6"; string _idkmore = "0.4"; string _hamless = "0.4"; string _spammore = "0.6";
            if (idkLessThanLimit_MaskedTextBox.Text != _idkless) { _idkless = $"{idkLessThanLimit_MaskedTextBox.Text}"; }
            if (idkMoreThanLimit_MaskedTextBox.Text != _idkmore) { _idkmore = $"{idkMoreThanLimit_MaskedTextBox.Text}"; }
            if (hamLessThanLimit_MaskedTextBox.Text != _hamless) { _hamless = $"{hamLessThanLimit_MaskedTextBox.Text}"; }
            if (spamMoreThanLimit_MaskedTextBox.Text != _spammore) { _spammore = $"{spamMoreThanLimit_MaskedTextBox.Text}"; }
            using (MemoryStream stream = new MemoryStream())
            {
                string json = $"{{\"hamless\":\"{_hamless}\",\"idkless\":\"{_idkless}\",\"idkmore\":\"{_idkmore}\",\"spammore\":\"{_spammore}\"}}";
                File.WriteAllText($@"{Application.StartupPath}\settings.json", json);
            }
            Thread.CurrentThread.CurrentCulture = temp_culture;
        }

        private void SpamMoreThanLimit_MaskedTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ',') { e.KeyChar = '.'; }
            if (spamMoreThanLimit_MaskedTextBox.SelectionStart == 0 & e.KeyChar == '.') { e.Handled = true; }
            if (spamMoreThanLimit_MaskedTextBox.SelectionStart == 0 & ((e.KeyChar >= '2') && (e.KeyChar <= '9'))) { e.Handled = true; }
            if (spamMoreThanLimit_MaskedTextBox.Text == "0") { if (e.KeyChar != '.' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (spamMoreThanLimit_MaskedTextBox.Text == "1") { if (e.KeyChar != '.' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (spamMoreThanLimit_MaskedTextBox.Text == "1.") { if (e.KeyChar != '0' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (e.KeyChar < '0' | e.KeyChar > '9' && e.KeyChar != (char)Keys.Back && e.KeyChar != '.') { e.Handled = true; }
            if (e.KeyChar == '.') { if (spamMoreThanLimit_MaskedTextBox.Text.IndexOf('.') != -1) { e.Handled = true; } }
        }

        private void HamLessThanLimit_MaskedTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ',') { e.KeyChar = '.'; }
            if (hamLessThanLimit_MaskedTextBox.SelectionStart == 0 & e.KeyChar == '.') { e.Handled = true; }
            if (hamLessThanLimit_MaskedTextBox.SelectionStart == 0 & ((e.KeyChar >= '2') && (e.KeyChar <= '9'))) { e.Handled = true; }
            if (hamLessThanLimit_MaskedTextBox.Text == "0") { if (e.KeyChar != '.' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (hamLessThanLimit_MaskedTextBox.Text == "1") { if (e.KeyChar != '.' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (hamLessThanLimit_MaskedTextBox.Text == "1.") { if (e.KeyChar != '0' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (e.KeyChar < '0' | e.KeyChar > '9' && e.KeyChar != (char)Keys.Back && e.KeyChar != '.') { e.Handled = true; }
            if (e.KeyChar == '.') { if (hamLessThanLimit_MaskedTextBox.Text.IndexOf('.') != -1) { e.Handled = true; } }
        }

        private void IdkMoreThanLimit_MaskedTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ',') { e.KeyChar = '.'; }
            if (idkMoreThanLimit_MaskedTextBox.SelectionStart == 0 & e.KeyChar == '.') { e.Handled = true; }
            if (idkMoreThanLimit_MaskedTextBox.SelectionStart == 0 & ((e.KeyChar >= '2') && (e.KeyChar <= '9'))) { e.Handled = true; }
            if (idkMoreThanLimit_MaskedTextBox.Text == "0") { if (e.KeyChar != '.' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (idkMoreThanLimit_MaskedTextBox.Text == "1") { if (e.KeyChar != '.' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (idkMoreThanLimit_MaskedTextBox.Text == "1.") { if (e.KeyChar != '0' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (e.KeyChar < '0' | e.KeyChar > '9' && e.KeyChar != (char)Keys.Back && e.KeyChar != '.') { e.Handled = true; }
            if (e.KeyChar == '.') { if (idkMoreThanLimit_MaskedTextBox.Text.IndexOf('.') != -1) { e.Handled = true; } }
        }

        private void IdkLessThanLimit_MaskedTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ',') { e.KeyChar = '.'; }
            if (idkLessThanLimit_MaskedTextBox.SelectionStart == 0 & e.KeyChar == '.') { e.Handled = true; }
            if (idkLessThanLimit_MaskedTextBox.SelectionStart == 0 & ((e.KeyChar >= '2') && (e.KeyChar <= '9'))) { e.Handled = true; }
            if (idkLessThanLimit_MaskedTextBox.Text == "0") { if (e.KeyChar != '.' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (idkLessThanLimit_MaskedTextBox.Text == "1") { if (e.KeyChar != '.' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (idkLessThanLimit_MaskedTextBox.Text == "1.") { if (e.KeyChar != '0' & e.KeyChar != (char)Keys.Back) { e.Handled = true; } }
            if (e.KeyChar < '0' | e.KeyChar > '9' && e.KeyChar != (char)Keys.Back && e.KeyChar != '.') { e.Handled = true; }
            if (e.KeyChar == '.') { if (idkLessThanLimit_MaskedTextBox.Text.IndexOf('.') != -1) { e.Handled = true; } }
        }

        private void Default_Button_Click(object sender, EventArgs e)
        {
            string _idkless = "0.6"; string _idkmore = "0.4"; string _hamless = "0.4"; string _spammore = "0.6";
            idkLessThanLimit_MaskedTextBox.Text = $"{_idkless}"; idkMoreThanLimit_MaskedTextBox.Text = $"{_idkmore}"; hamLessThanLimit_MaskedTextBox.Text = $"{_hamless}"; spamMoreThanLimit_MaskedTextBox.Text = $"{_spammore}";
            using (MemoryStream stream = new MemoryStream())
            {
                string json = $"{{\"hamless\":\"{_hamless}\",\"idkless\":\"{_idkless}\",\"idkmore\":\"{_idkmore}\",\"spammore\":\"{_spammore}\"}}";
                File.WriteAllText($@"{Application.StartupPath}\settings.json", json);
            }
        }
    }
}
