﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using MailKit.Net.Imap;
using MailKit.Net.Pop3;
using MailKit;
using System.Text.RegularExpressions;
using MimeKit;
using System.IO;
using System.Collections.Generic;

namespace Duplom
{
    public partial class Download_Form : Form
    {
        public Download_Form()
        {
            InitializeComponent();
        }        

        private void DownloadMail_Button_Click(object sender, EventArgs e)
        {
            if (usePop3_RadioButton.Checked == true)
            {
                using (var client = new Pop3Client())
                {
                    string server = mailServer_TextBox.Text;
                    client.Connect(server, 995, true);

                    string name = email_TextBox.Text;
                    string pass = password_TextBox.Text;

                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    client.Authenticate(name, pass);

                    pass = " ";

                    progressBar1.Maximum = client.Count;
                    progressBar1.Visible = true;

                    for (int i = 0; i < client.Count; i++)
                    {
                        progressBar1.Value = i;
                        var message = client.GetMessage(i);
                        var regex = @"\W";
                        if (message.Subject != null)
                        {
                            var reg = new Regex(regex);
                            var sub = reg.Replace(message.Subject, " ");
                            message.WriteTo($@"{Application.StartupPath}\tmp\{sub}.eml");
                            var body = message.BodyParts.OfType<TextPart>().FirstOrDefault();
                            Classifier core = new Classifier();
                            core.Test(body.Text);
                            Main_Form main = this.Owner as Main_Form;
                            textBox1.Text = body.Text;

                            if (core.Test(body.Text) == "SPAM")
                            {
                                textBox2.Text = "SPAM";
                                if (File.Exists($@"{Application.StartupPath}\Spam\{sub}.eml"))
                                {
                                    Random random = new Random();
                                    int randomNumber = random.Next(0, 100);
                                    File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Spam\{randomNumber}_{sub}_{randomNumber}.eml");
                                }
                                else { File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Spam\{sub}.eml"); }
                            }
                            else if (core.Test(body.Text) == "HAM")
                            {
                                textBox2.Text = "HAM";
                                if (File.Exists($@"{Application.StartupPath}\Inbox\{sub}.eml"))
                                {
                                    Random random = new Random();
                                    int randomNumber = random.Next(0, 100);
                                    File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Inbox\{randomNumber}_{sub}_{randomNumber}.eml");
                                }
                                else { File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Inbox\{sub}.eml"); }
                            }
                            else if (core.Test(body.Text) == "IDK")
                            {
                                textBox2.Text = "IDK";
                                if (File.Exists($@"{Application.StartupPath}\Inbox\{sub}.eml"))
                                {
                                    Random random = new Random();
                                    int randomNumber = random.Next(0, 100);
                                    File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Inbox\{randomNumber}_{sub}_{randomNumber}.eml");
                                }
                                else { File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Inbox\{sub}.eml"); }
                            }
                            main.textBox2.Text = textBox2.Text;
                            main.textBox1.Text = textBox1.Text;

                        }
                    }
                    progressBar1.Visible = false;
                    client.Disconnect(true);
                }
            } else if (useImap4_RadioButton.Checked == true)
            {
                using (var client = new ImapClient())
                {
                    using (var cancel = new CancellationTokenSource())
                    {
                        string server = mailServer_TextBox.Text;

                        client.Connect(server, 993, true, cancel.Token);

                        string name = email_TextBox.Text;
                        string pass = password_TextBox.Text;

                        client.Authenticate(name, pass, cancel.Token);

                        pass = " ";

                        var inbox = client.Inbox;
                        inbox.Open(FolderAccess.ReadOnly, cancel.Token);

                        progressBar1.Maximum = inbox.Count;
                        progressBar1.Visible = true;

                        for (int i = 0; i < inbox.Count; i++)
                        {
                            progressBar1.Value = i;
                            var message = inbox.GetMessage(i, cancel.Token);
                            var regex = @"\W";
                            if (message.Subject != null)
                            {
                                var reg = new Regex(regex);
                                var sub = reg.Replace(message.Subject, " ");
                                message.WriteTo($@"{Application.StartupPath}\tmp\{sub}.eml");
                                var body = message.BodyParts.OfType<TextPart>().FirstOrDefault();
                                Classifier core = new Classifier();
                                core.Test(body.Text);
                                Main_Form main = this.Owner as Main_Form;
                                textBox1.Text = body.Text;

                                if (core.Test(body.Text) == "SPAM")
                                {
                                    textBox2.Text = "SPAM";
                                    if (File.Exists($@"{Application.StartupPath}\Spam\{sub}.eml"))
                                    {
                                        Random random = new Random();
                                        int randomNumber = random.Next(0, 100);
                                        File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Spam\{randomNumber}_{sub}_{randomNumber}.eml");
                                    }
                                    else { File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Spam\{sub}.eml"); }
                                }
                                else if (core.Test(body.Text) == "HAM")
                                {
                                    textBox2.Text = "HAM";
                                    if (File.Exists($@"{Application.StartupPath}\Inbox\{sub}.eml"))
                                    {
                                        Random random = new Random();
                                        int randomNumber = random.Next(0, 100);
                                        File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Inbox\{randomNumber}_{sub}_{randomNumber}.eml");
                                    }
                                    else { File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Inbox\{sub}.eml"); }
                                }
                                else
                                {
                                    textBox2.Text = "IDK";
                                    if (File.Exists($@"{Application.StartupPath}\Inbox\{sub}.eml"))
                                    {
                                        Random random = new Random();
                                        int randomNumber = random.Next(0, 100);
                                        File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Inbox\{randomNumber}_{sub}_{randomNumber}.eml");
                                    }
                                    else { File.Move($@"{Application.StartupPath}\tmp\{sub}.eml", $@"{Application.StartupPath}\Inbox\{sub}.eml"); }
                                }
                                main.textBox2.Text = textBox2.Text;
                                main.textBox1.Text = textBox1.Text;
                            }
                        }
                        progressBar1.Visible = false;
                        client.Disconnect(true, cancel.Token);
                    }
                }
            }
            else { MessageBox.Show("Не вибрано протокол"); }
            Close();
        }
        public class RegexParser
        {
            public IEnumerable<string> Parse(string s) => Regex.Matches(s, @"\w+(\-\w+|\'\w+)*", RegexOptions.Compiled).Cast<Match>().Select(x => x.Value);
        }
    }
}