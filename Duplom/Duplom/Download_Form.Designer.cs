﻿namespace Duplom
{
    partial class Download_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Download_Form));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.usePop3_RadioButton = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.useImap4_RadioButton = new System.Windows.Forms.RadioButton();
            this.downloadMail_Button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.password_TextBox = new System.Windows.Forms.TextBox();
            this.email_TextBox = new System.Windows.Forms.TextBox();
            this.mailServer_TextBox = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.usePop3_RadioButton);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.useImap4_RadioButton);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(12, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(514, 49);
            this.groupBox1.TabIndex = 45;
            this.groupBox1.TabStop = false;
            // 
            // usePop3_RadioButton
            // 
            this.usePop3_RadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.usePop3_RadioButton.AutoSize = true;
            this.usePop3_RadioButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.usePop3_RadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.usePop3_RadioButton.Location = new System.Drawing.Point(177, 23);
            this.usePop3_RadioButton.Name = "usePop3_RadioButton";
            this.usePop3_RadioButton.Size = new System.Drawing.Size(67, 24);
            this.usePop3_RadioButton.TabIndex = 34;
            this.usePop3_RadioButton.TabStop = true;
            this.usePop3_RadioButton.Text = "POP3";
            this.usePop3_RadioButton.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Location = new System.Drawing.Point(82, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(412, 20);
            this.label4.TabIndex = 36;
            this.label4.Text = "Для завантаження пошти викоистовувати протокол:";
            // 
            // useImap4_RadioButton
            // 
            this.useImap4_RadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.useImap4_RadioButton.AutoSize = true;
            this.useImap4_RadioButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.useImap4_RadioButton.Checked = true;
            this.useImap4_RadioButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.useImap4_RadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.useImap4_RadioButton.Location = new System.Drawing.Point(274, 22);
            this.useImap4_RadioButton.Name = "useImap4_RadioButton";
            this.useImap4_RadioButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.useImap4_RadioButton.Size = new System.Drawing.Size(74, 24);
            this.useImap4_RadioButton.TabIndex = 35;
            this.useImap4_RadioButton.TabStop = true;
            this.useImap4_RadioButton.Text = "IMAP4";
            this.useImap4_RadioButton.UseVisualStyleBackColor = true;
            // 
            // downloadMail_Button
            // 
            this.downloadMail_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.downloadMail_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.downloadMail_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.downloadMail_Button.Location = new System.Drawing.Point(217, 140);
            this.downloadMail_Button.Name = "downloadMail_Button";
            this.downloadMail_Button.Size = new System.Drawing.Size(187, 29);
            this.downloadMail_Button.TabIndex = 44;
            this.downloadMail_Button.Text = "Завантажити листи";
            this.downloadMail_Button.UseVisualStyleBackColor = true;
            this.downloadMail_Button.Click += new System.EventHandler(this.DownloadMail_Button_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(8, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 20);
            this.label3.TabIndex = 43;
            this.label3.Text = "Пароль";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(9, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 20);
            this.label2.TabIndex = 42;
            this.label2.Text = "Електронна адреса";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(9, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 41;
            this.label1.Text = "Сервер";
            // 
            // password_TextBox
            // 
            this.password_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.password_TextBox.Location = new System.Drawing.Point(175, 56);
            this.password_TextBox.Name = "password_TextBox";
            this.password_TextBox.Size = new System.Drawing.Size(351, 26);
            this.password_TextBox.TabIndex = 40;
            this.password_TextBox.UseSystemPasswordChar = true;
            // 
            // email_TextBox
            // 
            this.email_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.email_TextBox.Location = new System.Drawing.Point(175, 30);
            this.email_TextBox.Name = "email_TextBox";
            this.email_TextBox.Size = new System.Drawing.Size(351, 26);
            this.email_TextBox.TabIndex = 39;
            // 
            // mailServer_TextBox
            // 
            this.mailServer_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mailServer_TextBox.Location = new System.Drawing.Point(175, 4);
            this.mailServer_TextBox.Name = "mailServer_TextBox";
            this.mailServer_TextBox.Size = new System.Drawing.Size(351, 26);
            this.mailServer_TextBox.TabIndex = 38;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 140);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(199, 29);
            this.progressBar1.TabIndex = 46;
            this.progressBar1.UseWaitCursor = true;
            this.progressBar1.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(496, 150);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(10, 20);
            this.textBox2.TabIndex = 48;
            this.textBox2.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(480, 150);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(10, 20);
            this.textBox1.TabIndex = 47;
            this.textBox1.Visible = false;
            // 
            // Download_Form
            // 
            this.ClientSize = new System.Drawing.Size(538, 182);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.downloadMail_Button);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.password_TextBox);
            this.Controls.Add(this.email_TextBox);
            this.Controls.Add(this.mailServer_TextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Download_Form";
            this.Text = "Завантажити повідомлення";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton usePop3_RadioButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton useImap4_RadioButton;
        private System.Windows.Forms.Button downloadMail_Button;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox password_TextBox;
        private System.Windows.Forms.TextBox email_TextBox;
        private System.Windows.Forms.TextBox mailServer_TextBox;
        private System.Windows.Forms.ProgressBar progressBar1;
        public System.Windows.Forms.TextBox textBox2;
        public System.Windows.Forms.TextBox textBox1;
    }
}