﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.Serialization;
using Accord.Statistics.Distributions.Univariate;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;
using System.Reflection;

namespace Duplom
{
    [DataContract] public class SaveInfo
    {
        [DataMember] public IDictionary<String, Int32> Spam { get; set; }
        [DataMember] public IDictionary<String, Int32> Ham { get; set; }
        [DataMember] public Int32 SpamCount { get; set; }
        [DataMember] public Int32 HamCount { get; set; }
    }

    public class Classifier
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main_Form());
        }

        private Int32 _spamDocCount;
        private Int32 _hamDocCount;
        readonly IDictionary<String, Int32> _spam = new Dictionary<String, Int32>();
        readonly IDictionary<String, Int32> _ham = new Dictionary<String, Int32>();

        public Classifier() {  }

        public Classifier(SaveInfo saveInfo)
        {
            _spam = saveInfo.Spam; _ham = saveInfo.Ham;
            _hamDocCount = saveInfo.HamCount; _spamDocCount = saveInfo.SpamCount;
        }

        public Classifier(IEnumerable<String> spamDoc, IEnumerable<String> hamDoc)
        {
            foreach (var s in spamDoc) { TagSpam(s); }
            foreach (var s in hamDoc) { TagHam(s); }

        }

        public void TeachSpam(String s)
        {
            var parser = new RegexParser();
            s = Regex.Replace(s, @"\w", m => m.ToString().ToLower());
            var words = parser.Parse(s);
            foreach (var word in words)
            {
                if (_spam.ContainsKey(word)) { _spam[word]++; } else { _spam.Add(word, 1); }
            }
            _spamDocCount++;
        }

        public void TeachHam(String s)
        {
            var parser = new RegexParser();
            s = Regex.Replace(s, @"\w", m => m.ToString().ToLower());
            var words = parser.Parse(s);
            foreach (var word in words)
            {
                if (_ham.ContainsKey(word)) { _ham[word]++; } else { _ham.Add(word, 1); }
            }
            _hamDocCount++;
        }

        public void TeachIdk(String s)
        {
            var parser = new RegexParser();
            s = Regex.Replace(s, @"\w", m => m.ToString().ToLower());
            var words = parser.Parse(s);
            foreach (var word in words)
            {
                if (_ham.ContainsKey(word)) { _ham[word]++; } else { _ham.Add(word, 1); }
                if (_spam.ContainsKey(word)) { _spam[word]++; } else { _spam.Add(word, 1); }
            }
            _hamDocCount++; _spamDocCount++;
        }

        public void TagSpam(String s)
        {
            var parser = new RegexParser();
            s = Regex.Replace(s, @"\w", m => m.ToString().ToLower());
            var words = parser.Parse(s);
            foreach (var word in words)
            {
                int value;
                _ham.TryGetValue(word, out value);
                if (_spam.ContainsKey(word)) { _spam[word]++; } else { _spam.Add(word, 1); }
                if (_ham.ContainsKey(word) &&  value > 0) { _ham[word]--; }
                if (_spam.ContainsKey(word) && value == 0) { _spam[word]++; }
            }
            _spamDocCount++;
            if (_hamDocCount <= 0) { _hamDocCount++; } else { _hamDocCount--; }
        }

        public void TagHam(String s)
        {
            var parser = new RegexParser();
            s = Regex.Replace(s, @"\w", m => m.ToString().ToLower());
            var words = parser.Parse(s);
            foreach (var word in words)
            {
                int value;
                _spam.TryGetValue(word, out value);
                if (_ham.ContainsKey(word)) { _ham[word]++; } else { _ham.Add(word, 1); }
                if (_spam.ContainsKey(word) && value > 0)  { _spam[word]--; }
                if (_spam.ContainsKey(word) && value == 0) { _spam[word]++; }
            }
             _hamDocCount++;
            if (_spamDocCount <= 0) { _spamDocCount++; } else { _spamDocCount--; }
        }

        public class RegexParser
        {
            public IEnumerable<string> Parse(string s) => Regex.Matches(s, @"\w+(\-\w+|\'\w+)*", RegexOptions.Compiled).Cast<Match>().Select(x => x.Value);
        }

        public Double SpamCountByFisher(String s)
        {
            var parser = new RegexParser();
            s = Regex.Replace(s, @"\w", m => m.ToString().ToLower());
            var words = parser.Parse(s);
            int count = 0; var hsum = 0.0; var ssum = 0.0;
            foreach (var word in words)
            {
                count++;
                float countham = 1; float b; float g; float p;
                float countspam = 1; float ss = 1; float xx = 0.5f;
                if (_spam.ContainsKey(word))
                    countspam += _spam[word];
                b = (float)(countspam / _spamDocCount);
                if (_ham.ContainsKey(word))
                    countham += _ham[word];
                g = (float)(countham / _hamDocCount);
                p = (b / (b + g));
                var f = ((ss * xx) + (countspam + countham) * p) / (ss + (countspam + countham));
                hsum += Math.Log(f);
                ssum += Math.Log(1 - f);
            }
            if (count == 0) { count = 1; }
            var hinvchisq = new InverseChiSquareDistribution(degreesOfFreedom: (2 * count));
            var h = hinvchisq.DistributionFunction(x: (-2 * hsum));
            var nhinvchisq = new InverseChiSquareDistribution(degreesOfFreedom: (2 * count));
            var nh = nhinvchisq.DistributionFunction(x: (-2 * ssum));
            var i = (1 + h - nh) / (2);
            return i;
        }

        public class RegexParserSettings
        {
            public IEnumerable<string> Parse(string s) => Regex.Matches(s, "\\d\\.\\d+", RegexOptions.Compiled).Cast<Match>().Select(x => x.Value);
        }

        public String Test(String s)
        {
            string _hamless = "0.4"; string _idkless = "0.6"; string _idkmore = "0.4"; string _spammore = "0.6";
            using (StreamReader sr = new StreamReader($@"{AppDomain.CurrentDomain.BaseDirectory}\settings.json"))
            {
                String line = sr.ReadToEnd();
                var parser = new RegexParserSettings();
                var words = parser.Parse(line);
                int count = 0;
                
                foreach (var word in words)
                {
                    count++;
                    if (count == 1) { _hamless = $"{word}"; }
                    if (count == 2) { _idkless = $"{word}"; }
                    if (count == 3) { _idkmore = $"{word}"; }
                    if (count == 4) { _spammore = $"{word}"; }
                }
            }

            CultureInfo temp_culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
            double hamless = double.Parse(_hamless);
            double idkless = double.Parse(_idkless);
            double idkmore = double.Parse(_idkmore);
            double spammore = double.Parse(_spammore);
            Thread.CurrentThread.CurrentCulture = temp_culture;

            var fisher = SpamCountByFisher(s);
            string tag = " ";
            if (fisher > spammore) { tag = "SPAM"; }
            else if (fisher < hamless) { tag = "HAM"; }
            else if (fisher >= idkmore && fisher <= idkless) { tag = "IDK"; }
            return String.Format($"{tag}");
        }

        public SaveInfo GetSaveObj()
        {
            return new SaveInfo
            {
                Spam = _spam,
                Ham = _ham,
                HamCount = _hamDocCount,
                SpamCount = _spamDocCount
            };
        }
    }
}
