﻿namespace Duplom
{
    partial class Settings_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings_Form));
            this.limitsOfProbability_GroupBox = new System.Windows.Forms.GroupBox();
            this.idkMoreThanLimit_MaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.hamLessThanLimit_MaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.idkLessThanLimit_MaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.spamMoreThanLimit_MaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.cangeLimitsOfProbability_CheckBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.save_Button = new System.Windows.Forms.Button();
            this.default_Button = new System.Windows.Forms.Button();
            this.limitsOfProbability_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // limitsOfProbability_GroupBox
            // 
            this.limitsOfProbability_GroupBox.Controls.Add(this.idkMoreThanLimit_MaskedTextBox);
            this.limitsOfProbability_GroupBox.Controls.Add(this.hamLessThanLimit_MaskedTextBox);
            this.limitsOfProbability_GroupBox.Controls.Add(this.idkLessThanLimit_MaskedTextBox);
            this.limitsOfProbability_GroupBox.Controls.Add(this.spamMoreThanLimit_MaskedTextBox);
            this.limitsOfProbability_GroupBox.Controls.Add(this.cangeLimitsOfProbability_CheckBox);
            this.limitsOfProbability_GroupBox.Controls.Add(this.label2);
            this.limitsOfProbability_GroupBox.Controls.Add(this.label3);
            this.limitsOfProbability_GroupBox.Controls.Add(this.label4);
            this.limitsOfProbability_GroupBox.Controls.Add(this.label5);
            this.limitsOfProbability_GroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.limitsOfProbability_GroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.limitsOfProbability_GroupBox.Location = new System.Drawing.Point(1, 12);
            this.limitsOfProbability_GroupBox.Name = "limitsOfProbability_GroupBox";
            this.limitsOfProbability_GroupBox.Size = new System.Drawing.Size(589, 183);
            this.limitsOfProbability_GroupBox.TabIndex = 51;
            this.limitsOfProbability_GroupBox.TabStop = false;
            this.limitsOfProbability_GroupBox.Text = "Межі  ймовірністей класифікації";
            // 
            // idkMoreThanLimit_MaskedTextBox
            // 
            this.idkMoreThanLimit_MaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.idkMoreThanLimit_MaskedTextBox.Enabled = false;
            this.idkMoreThanLimit_MaskedTextBox.Location = new System.Drawing.Point(509, 122);
            this.idkMoreThanLimit_MaskedTextBox.Name = "idkMoreThanLimit_MaskedTextBox";
            this.idkMoreThanLimit_MaskedTextBox.ShortcutsEnabled = false;
            this.idkMoreThanLimit_MaskedTextBox.Size = new System.Drawing.Size(33, 26);
            this.idkMoreThanLimit_MaskedTextBox.TabIndex = 37;
            this.idkMoreThanLimit_MaskedTextBox.Text = "0.4";
            this.idkMoreThanLimit_MaskedTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IdkMoreThanLimit_MaskedTextBox_KeyPress);
            // 
            // hamLessThanLimit_MaskedTextBox
            // 
            this.hamLessThanLimit_MaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hamLessThanLimit_MaskedTextBox.Enabled = false;
            this.hamLessThanLimit_MaskedTextBox.Location = new System.Drawing.Point(464, 86);
            this.hamLessThanLimit_MaskedTextBox.Name = "hamLessThanLimit_MaskedTextBox";
            this.hamLessThanLimit_MaskedTextBox.ShortcutsEnabled = false;
            this.hamLessThanLimit_MaskedTextBox.Size = new System.Drawing.Size(33, 26);
            this.hamLessThanLimit_MaskedTextBox.TabIndex = 36;
            this.hamLessThanLimit_MaskedTextBox.Text = "0.4";
            this.hamLessThanLimit_MaskedTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HamLessThanLimit_MaskedTextBox_KeyPress);
            // 
            // idkLessThanLimit_MaskedTextBox
            // 
            this.idkLessThanLimit_MaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.idkLessThanLimit_MaskedTextBox.Enabled = false;
            this.idkLessThanLimit_MaskedTextBox.Location = new System.Drawing.Point(509, 150);
            this.idkLessThanLimit_MaskedTextBox.Name = "idkLessThanLimit_MaskedTextBox";
            this.idkLessThanLimit_MaskedTextBox.ShortcutsEnabled = false;
            this.idkLessThanLimit_MaskedTextBox.Size = new System.Drawing.Size(33, 26);
            this.idkLessThanLimit_MaskedTextBox.TabIndex = 35;
            this.idkLessThanLimit_MaskedTextBox.Text = "0.6";
            this.idkLessThanLimit_MaskedTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IdkLessThanLimit_MaskedTextBox_KeyPress);
            // 
            // spamMoreThanLimit_MaskedTextBox
            // 
            this.spamMoreThanLimit_MaskedTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spamMoreThanLimit_MaskedTextBox.Enabled = false;
            this.spamMoreThanLimit_MaskedTextBox.Location = new System.Drawing.Point(444, 56);
            this.spamMoreThanLimit_MaskedTextBox.Name = "spamMoreThanLimit_MaskedTextBox";
            this.spamMoreThanLimit_MaskedTextBox.ShortcutsEnabled = false;
            this.spamMoreThanLimit_MaskedTextBox.Size = new System.Drawing.Size(33, 26);
            this.spamMoreThanLimit_MaskedTextBox.TabIndex = 34;
            this.spamMoreThanLimit_MaskedTextBox.Text = "0.6";
            this.spamMoreThanLimit_MaskedTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SpamMoreThanLimit_MaskedTextBox_KeyPress);
            // 
            // cangeLimitsOfProbability_CheckBox
            // 
            this.cangeLimitsOfProbability_CheckBox.AutoSize = true;
            this.cangeLimitsOfProbability_CheckBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cangeLimitsOfProbability_CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cangeLimitsOfProbability_CheckBox.Location = new System.Drawing.Point(6, 19);
            this.cangeLimitsOfProbability_CheckBox.Name = "cangeLimitsOfProbability_CheckBox";
            this.cangeLimitsOfProbability_CheckBox.Size = new System.Drawing.Size(323, 24);
            this.cangeLimitsOfProbability_CheckBox.TabIndex = 25;
            this.cangeLimitsOfProbability_CheckBox.Text = "Змінити межі ймовірністей класифікації";
            this.cangeLimitsOfProbability_CheckBox.UseVisualStyleBackColor = true;
            this.cangeLimitsOfProbability_CheckBox.CheckedChanged += new System.EventHandler(this.CangeLimitsOfProbability_CheckBox_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Location = new System.Drawing.Point(18, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(435, 20);
            this.label2.TabIndex = 26;
            this.label2.Text = "Вважати лист спамом якщо він має ймовірність більше: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Location = new System.Drawing.Point(18, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(451, 20);
            this.label3.TabIndex = 27;
            this.label3.Text = "Вважати лист не спамом якщо він має ймовірність менше:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Location = new System.Drawing.Point(18, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(494, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "Вважати лист сумнівним якщо його ймовірність більше рівне за:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Location = new System.Drawing.Point(362, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 20);
            this.label5.TabIndex = 33;
            this.label5.Text = "та менше рівне за:";
            // 
            // save_Button
            // 
            this.save_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.save_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.save_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.save_Button.Location = new System.Drawing.Point(23, 201);
            this.save_Button.Name = "save_Button";
            this.save_Button.Size = new System.Drawing.Size(142, 32);
            this.save_Button.TabIndex = 49;
            this.save_Button.Text = "Зберегти";
            this.save_Button.UseVisualStyleBackColor = true;
            this.save_Button.Click += new System.EventHandler(this.Save_Button_Click);
            // 
            // default_Button
            // 
            this.default_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.default_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.default_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.default_Button.Location = new System.Drawing.Point(367, 201);
            this.default_Button.Name = "default_Button";
            this.default_Button.Size = new System.Drawing.Size(169, 32);
            this.default_Button.TabIndex = 54;
            this.default_Button.Text = " За Замовчуванням";
            this.default_Button.UseVisualStyleBackColor = true;
            this.default_Button.Click += new System.EventHandler(this.Default_Button_Click);
            // 
            // Settings_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 245);
            this.Controls.Add(this.default_Button);
            this.Controls.Add(this.limitsOfProbability_GroupBox);
            this.Controls.Add(this.save_Button);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Settings_Form";
            this.Text = "Налаштування";
            this.limitsOfProbability_GroupBox.ResumeLayout(false);
            this.limitsOfProbability_GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox limitsOfProbability_GroupBox;
        private System.Windows.Forms.CheckBox cangeLimitsOfProbability_CheckBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button save_Button;
        private System.Windows.Forms.MaskedTextBox spamMoreThanLimit_MaskedTextBox;
        private System.Windows.Forms.MaskedTextBox idkMoreThanLimit_MaskedTextBox;
        private System.Windows.Forms.MaskedTextBox hamLessThanLimit_MaskedTextBox;
        private System.Windows.Forms.MaskedTextBox idkLessThanLimit_MaskedTextBox;
        private System.Windows.Forms.Button default_Button;
    }
}